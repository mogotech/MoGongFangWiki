TP5 paginate()分页后给结果集追加字段和数据
在TP5中，我们常常会需要做数据分页查询，但是只查询得到的数据并不能完全满足我们的需求，我们偶尔会想数据集中追加我们需要的指定的字段和数据。

在TP官网中找了很久没招到相关资料，最后去翻开发手册后完美解决。


下面贴出解决方案：

public function index(){
    $sql = "";
    $list = "";
    $pagenumber = 20;//默认分页条数

    //查询数据
    $list = Db::name('wcmall_type','id,name,sort')->where($sql)->order('sort asc')->paginate($pagenumber,false,['query'=>request()->param()])->each(function($item, $key){
        $wctypeid = $item["id"]; //获取数据集中的id
        $num = Db::name('wcmall_type_attribute')->where("wctypeid='$wctypeid'")->count('id'); //根据ID查询相关其他信息
        $item['num'] = $num; //给数据集追加字段num并赋值
        return $item;
    });
    $page = $list->render();
    //输出到模板
    return view('type/index',['list'=>$list,'page'=>$page,'title'=>'商品类型']);

}