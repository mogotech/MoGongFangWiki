### 好处
+ 减少请求
+ 减少文件体积
+ 资源文件统一管理
+ CDN加速
### 使用方法
#### 后端 或者 七牛

在tengine配置项中打开contact模块
```
   location /static/ {
    concat on;
    concat_max_files 20;
}
```
具体配置项可参考 [ngx_http_concat_module](http://tengine.taobao.org/document_cn/http_concat_cn.html)
七牛的配置可以看 [七牛文本合并](https://developer.qiniu.com/dora/manual/1253/text-file-merging-concat)
#### 前端
公司已搭建cdn资源系统,可以将资源先上传至cdn,然后通过请求压缩
[魔工坊CDN](http://api.mogo.club/admin.php)
```
<script  type="text/javascript"  src="http://api.femirror.com/static/admin/js/core/??jquery.min.js,bootstrap.min.js,jquery.slimscroll.min.js,jquery.scrollLock.min.js,jquery.appear.min.js,jquery.countTo.min.js,jquery.placeholder.min.js,js.cookie.min.js"></script>
```
上述就是将各个js文件合并成一个文件