title: 魔工坊周报[20171230]
---
#在html项目中使用es6,react
## 优点
+ 可以在es5浏览器中使用es6,react框架.
+ 使用es6,甚至es7的新特性
## 原理
使用bable将es5,jsx转译成es5语法执行.
## demo
这个实例中的 weui jquery 仍然可以正常执行.
```html
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8" />
  <title>React Tutorial</title>
  <meta name="viewport" content="initial-scale=1, maximum-scale=1">
  <meta name="apple-mobile-web-app-capable" content="yes">
  <meta name="apple-mobile-web-app-status-bar-style" content="black">
  <link rel="stylesheet" href="https://cdn.bootcss.com/weui/1.1.2/style/weui.min.css">
  <link rel="stylesheet" href="https://cdn.bootcss.com/jquery-weui/1.2.0/css/jquery-weui.min.css">
  <script src="https://unpkg.com/react@15/dist/react.min.js"></script>
  <script src="https://unpkg.com/react-dom@15/dist/react-dom.min.js"></script>
  <script src="https://cdn.bootcss.com/babel-core/5.8.23/browser.min.js"></script>
</head>
<body>
    <div class="weui-navbar">
        <div class="weui-navbar__item weui_bar__item_on">
          选项一
        </div>
        <div class="weui-navbar__item">
          选项二
        </div>
        <div class="weui-navbar__item">
          选项三
        </div>
      </div>
  <div id="app"></div>
  <script src="https://cdn.bootcss.com/jquery/1.11.0/jquery.min.js"></script>
  <script src="https://cdn.bootcss.com/jquery-weui/1.2.0/js/jquery-weui.min.js"></script>

  <script type="text/babel">
        var MessageBox = React.createClass({
            render:function(){
              // 发送ajax请求
              $.ajax({
                urk:'https://api.douban.com/v1/book/search?q=%E7%88%B1',
                type:'get',
              }).then((res)=>{
                // console.log(res)
              })
              //下面的代码任然可以使用
              // $.actions({
              //   actions: [{
              //     text: "编辑",
              //     onClick: function() {
              //       //do something
              //     }
              //   },{
              //     text: "删除",
              //     onClick: function() {
              //       //do something
              //     }
              //   }]
              // });
              var arr=['西瓜','茄子','香蕉'];
                return (
                  <div className="weui-cells">
                    {arr.map((item)=>{
                        return (
                          <div className="weui-cell">
                              <div className="weui-cell__bd">
                                <p>{item}</p>
                              </div>
                          <div className="weui-cell__ft">说明文字</div>
                        </div>)
                    })}
                  </div>
                )
            }
        });
        ReactDOM.render(
          <MessageBox />  ,
      document.getElementById('app')
);
</script>
</body>
</html>
```

如果不会react [React教程](https://doc.react-china.org/docs/hello-world.html)