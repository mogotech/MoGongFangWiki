title: WeUI
-----------
## 前言
前端统一以WeUI作为框架,使设计达到风格统一化.
## 文档
[WeUI使用文档](https://jqweui.cn/)
## 快速开始
其中CDN版本变动请联系管理员
### 页面结构
```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.bootcss.com/weui/1.1.2/style/weui.min.css">
    <link rel="stylesheet" href="https://cdn.bootcss.com/jquery-weui/1.2.0/css/jquery-weui.min.css">
</head>
<body>
 // 业务代码
<script src="https://cdn.bootcss.com/jquery/1.11.0/jquery.min.js"></script>
<script src="https://cdn.bootcss.com/jquery-weui/1.2.0/js/jquery-weui.min.js"></script>
</body>
</html>
```

### 额外资源
```html
<!-- 如果使用了某些拓展插件还需要额外的JS -->
<script src="https://cdn.bootcss.com/jquery-weui/1.2.0/js/swiper.min.js"></script>
<script src="https://cdn.bootcss.com/jquery-weui/1.2.0/js/city-picker.min.js"></script>
```
## 微信JS-SDK
### [微信JS-SDK文档](https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421141115)
### 引入资源
```html
<!--  如果引用了微信jsSDK-->
<script src="https://res.wx.qq.com/open/js/jweixin-1.2.0.js"></script>
```
{% note info %}
如果生产中有需要购买付费文档,可以联系管理员统一采购
{% endnote %}