title: 海豚PHP
------------
## 前言
DophinPHP（海豚PHP）是一个基于ThinkPHP5.X开发的一套开源PHP快速开发框架，
## 版本
目前使用的是海豚基带的版本,一切以海豚php为准.*看文档需注意版本约束*.
## 文档

* [海豚文档](https://www.kancloud.cn/ming5112/dolphinphp/256299)

## 初始化项目
在码云初始化项目时使用这个初始化,这个版本里面修改了版权信息,增加了常用插件
> https://gitee.com/mogotech/MoGongFangDolphinPHP.git

## 快速开始
### 控制器
+ 统一使用 $this->error()/success() 返回json数据.
+ 避免直接使用session对状态进行操作,统一写成公共方法.
### 模型
+ 涉及增删改,单条关联数据查询的数据表都需要有一一对应的关联模型
+ 涉及条件范围多项查询的使用视图查询
+ 非关联表必须使用软删除
+ 善于使用范围查询,尤其是用范围查询检测数据权限

软删除必须在建表时 加入
 |-|-|
 |||
### 验证器
+ 不要相信前端给的任何数据
+ 在模型和控制器中都可以使用验证器

### 海豚表格
### 海豚表单



## 下载
[海豚最新版本php下载](http://www.dolphinphp.com/getdolphin.html)

`版权修改版本正在制作中...`
## 插件
* [阿里短信插件下载](http://cdn.mogo.club/AliyunSms.zip)
* [阿里短信插件使用](http://bbs.dolphinphp.com/?/article/63)
* [七牛插件下载(已经修复问题)](http://cdn.mogo.club/QiNiu.zip)
* [七牛插件使用](http://bbs.dolphinphp.com/?/article/48)

{% note info %}
如果生产中有需要购买付费文档,可以联系管理员统一采购
{% endnote %}

## 附录