title: ThinkPHP
---------------
## 前言
公司采用的后台架构是`thinkphp`,`dolphinphp`,`mysql`,thinkphp作为重要的一环需要熟悉掌握整个框架的特性与功能.

## 版本
目前使用的是海豚基带的版本,一切以海豚php为准.*看文档需注意版本约束*.
## 文档
除了基本文档,公司已购买已购买付费文档,有需要可以联系管理员阅读
* [ThinkPHP5文档](https://www.kancloud.cn/manual/thinkphp5/118003)
* [ThinkPHP5.0控制器从入门到精通](https://www.kancloud.cn/thinkphp/controller-in-detail)
* [掌握ThinkPHP5.0数据库和模型](https://www.kancloud.cn/thinkphp/master-database-and-model)

{% note info %}
如果生产中有需要购买付费文档,可以联系管理员统一采购
{% endnote %}

